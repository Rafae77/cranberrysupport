﻿par Gabriel Boisvert et Raphael David Higuera Silva

Documentez votre analyse dans ce fichier.

En Général


1.Comme mentionné dans l’énocé, à la place de garder les données dans la base de données, il faudra les garder dans une base de données comme MySql, 
puis ajouter les librairies nécessaires pour sortir et sauver l’information dans la base de données.  

Code smells.

2.Le problème de les méthodes larges (long method), qui est une méthode qui contient beaucoup de code. 
Cela devient inlisible et probablement source de bugs. La solution est extraction de méthode, donc prendre une partie du code qui pourrait être une méthode apart.

3.Le problème de classes larges(large class) qui empêche la lisibilité de celui qui va lire le code et qui est surement peut causer une source de bugs.  
La solution la plus courante est l’extraction de classe qui va prendre une partie de classe qui peut être sa propre classe. On peut faire aussi des sous-classe. 

4.Le problème de l’utilisation recurrente de variables primitifs qui fait que le code ne soit pas lisible et qui est encore une fois source de bugs. 
La solution pour les primitifs est de les regrouper les variables qui sont logiques d’être d’une même classe puis créer cette classe est les mettres dans la classe.
 Ceci va faire que le code soit plus comprehensible à cause des noms des classes qui sont plus claire que le nom d’une variable. 

5.Les méthodes qui ont plus que 3 variables comme paramètres peut causer une difficulté de plus pour le lecteur du code.
Une des solutions est à la place de mettre un paramètre qui a été donné par une méthode, effacer le paramètre et à la place appeler la méthode dans la méthode. 
On peut aussi a la place de passer plusieurs variables comme paramètres, on peut passer juste l’objet au complet. 

6.La duplication du code peut être un problème, car si on modifie une partie il va falloir chercher dans tout les fichiers pour changer l’autre partie pour que le code fonctionne correctement. 
Les solutions les plus logiques sont sortir la partie du code dans une méthode ou dans une classe, mais l’idée est que il n’y a pas de code répéter pour ne pas avoir un problème de consistence. 

7.Un problème qui est très visible est le même de ‘packages’ dans le projet, se qui fait que sa soit inlisible et qu’on doit chercher dans toutes les classes pour trouver la classe qu’on a besoin. 
Donc, la solution est le regroupement logique des classes dans des packages.

8.Le problème des ‘switch statements’ qui fait que le code soit incomprehinsible à cause de la longueur du ‘switch’. 
La soltuion est le polymorphisme, donc faire une extraction de méthode puis le déplacement de cette méthode dans la classe qui logiquement la ‘switch’ devrait être. 
On peut aussi faire le ‘refactoring :     replace contidional with polymorphism’. 

9.Il y a beaucoup de commentaires qui polluent le code et qui fait que sa soit plus difficile que facile de lire le code. 
La solution serait l’extraction de méthode et mettre comme nom le commentaire qui était avant. 

10.Une petite classe qui ne fait rien peut être transformer à une classe interne.

11.Remplacer les null avec des null objet, pour ne pas avoir besoin de faire : objet == null.

Refactoring

12.Bien indenter le code, utiliser des noms de variables et de méthodes qui sont signifiants et facile a comprendre. 

13.Implèmenter des tests qui s’assure constamment de la fonctionnalité du code.

14.Uniformiser le code, cela facilite la lecteur.  

par Gabriel Boisvert et Raphael David Higuera Silva

Documentez votre analyse dans ce fichier.

En Général


1.Comme mentionné dans l’énocé, à la place de garder les données dans la base de données, il faudra les garder dans une base de données comme MySql, 
puis ajouter les librairies nécessaires pour sortir et sauver l’information dans la base de données.  

Code smells.

2.Le problème de les méthodes larges (long method), qui est une méthode qui contient beaucoup de code. 
Cela devient inlisible et probablement source de bugs. La solution est extraction de méthode, donc prendre une partie du code qui pourrait être une méthode apart.

3.Le problème de classes larges(large class) qui empêche la lisibilité de celui qui va lire le code et qui est surement peut causer une source de bugs.  
La solution la plus courante est l’extraction de classe qui va prendre une partie de classe qui peut être sa propre classe. On peut faire aussi des sous-classe. 

4.Le problème de l’utilisation recurrente de variables primitifs qui fait que le code ne soit pas lisible et qui est encore une fois source de bugs. 
La solution pour les primitifs est de les regrouper les variables qui sont logiques d’être d’une même classe puis créer cette classe est les mettres dans la classe.
 Ceci va faire que le code soit plus comprehensible à cause des noms des classes qui sont plus claire que le nom d’une variable. 

5.Les méthodes qui ont plus que 3 variables comme paramètres peut causer une difficulté de plus pour le lecteur du code.
Une des solutions est à la place de mettre un paramètre qui a été donné par une méthode, effacer le paramètre et à la place appeler la méthode dans la méthode. 
On peut aussi a la place de passer plusieurs variables comme paramètres, on peut passer juste l’objet au complet. 

6.La duplication du code peut être un problème, car si on modifie une partie il va falloir chercher dans tout les fichiers pour changer l’autre partie pour que le code fonctionne correctement. 
Les solutions les plus logiques sont sortir la partie du code dans une méthode ou dans une classe, mais l’idée est que il n’y a pas de code répéter pour ne pas avoir un problème de consistence. 

7.Un problème qui est très visible est le même de ‘packages’ dans le projet, se qui fait que sa soit inlisible et qu’on doit chercher dans toutes les classes pour trouver la classe qu’on a besoin. 
Donc, la solution est le regroupement logique des classes dans des packages.

8.Le problème des ‘switch statements’ qui fait que le code soit incomprehinsible à cause de la longueur du ‘switch’. 
La soltuion est le polymorphisme, donc faire une extraction de méthode puis le déplacement de cette méthode dans la classe qui logiquement la ‘switch’ devrait être. 
On peut aussi faire le ‘refactoring :     replace contidional with polymorphism’. 

9.Il y a beaucoup de commentaires qui polluent le code et qui fait que sa soit plus difficile que facile de lire le code. 
La solution serait l’extraction de méthode et mettre comme nom le commentaire qui était avant. 

10.Une petite classe qui ne fait rien peut être transformer à une classe interne.

11.Remplacer les null avec des null objet, pour ne pas avoir besoin de faire : objet == null.

Refactoring

12.Bien indenter le code, utiliser des noms de variables et de méthodes qui sont signifiants et facile a comprendre. 

13.Implèmenter des tests qui s’assure constamment de la fonctionnalité du code.

14.Uniformiser le code, cela facilite la lecteur.  



Dans la classe Utilisateur:
	- Certain champ ne sont pas utiliser et seront enlever (telephone, mail, bureau). (cohésion)
	- Meilleur encapsulation du champ rôle, passer de public à protected
	- Le rôle passer en paramètre au constructeur ne sert a rien, on définira le role dans la méthode avec une valeur définit "client"
	- La méthode fechInfo n'est jamais utilisé et peux donc être enlever, ainsi que l'attribut info
	- La methode getNom retourne le même résultat que la méthode getNomUtil, l'une d'entre elle sera supprimé
	- La condition de la méthode login peut être simplifier en retournant directement le résultat de la condition. (replace conditionnal with query)
	- La méthode getListStatut n'est jamais apellé et peut être enlevé
	- Ajout d'une ArrayList qui contiendrais la liste de requete perso
	- Enlever la méthode getListStatut, car elle est jamais appelé
	
Dans la classe Client
	- Monter d'un niveau (Utilisateur) l'attribut listeRequetesClient. (move up field)
	- Fusionné la classe avec celle d'utilisateur, ainsi avoir une classe Utilisateur qui sera client par défaut et ne contenant pas de méthod abstract
	
Dans la classe Technicient
	- Enlever l'importation de la classe Requete.status qui n'est jamais utilisé
	- Enlever l'Attribut duplication listRequetesPerso et listRequetesTechs, car ils sont hérité d'Utilisateur.
	- Remplacer les conditions dans la méthode getRequeteParStatus avec le résultat de la longeur de l'ArraysList concerner (status("entraitement") == listeRequeteEnCours.size()). (replace conditionnal with query)

Dans la classe Commentaire
	- Changer l'encapsulation protected a private
	
Dans la classe Requete
	- Import FileNotFoundException inutilisé
	- Supprimer les attribut client et technicien remplacer par un seul attribut utilisateur
	- Supprimer le constructeur dupliqué, car l'attribut client devient Utilisateur
	- L'attribut Utilisateur tempo peut être remplacer par une variable temporaire
	- Lancement d'exception dupliqué dans le constructeur
	- IOException est plus spécifique que FileNotFoundException
	- Ajout d'un paramètre fichier au constructeur
	- Suppression de la méthode finaliser jamais utilisé 
	- Déplacement de la méthode setTech dans Technicien
	
Dans la classe BanqueUtilisateurs
	- changement de l'encapsulation de méthode chargerUtilisateur et addUtilisateur, il sont devenu private
	- Mofication de la méthode chercherUtilisateur, si un Utilisateur est passer en paramètre ses qu'on ne le cherche plus 
	- Simplification de la méthode getUserInstance
	
Dans la classe BanqueRequetes
	- Suppression de la constante initial_requests, car une ArrayList s'initialise déjà à 100 par défaut
	- Enlever l'exception FileNotFoundException dans le constructeur
	- Simplification de la méthode getInstance
	- Ajout d'une méthode pour créer une requete avec un fichier en paramètre
	- Implementation de la base de donnée + autre exigence
	
Dans la classe ClientLoggedFrame
	- Suppression de l'attribut String client name qui n'est jamais utilisé

Dans la classe TechLoggedFrame
	- Suppression de l'attribut String client name qui n'est jamais utilisé
	
Dans toute les classes
	- Établir un standart d'écriture
	- Implémentaton des exigences
	