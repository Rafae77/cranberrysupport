package testServices;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;

import models.Utilisateur;
import services.BanqueUtilisateurs;

public class TestBanqueUtilisteurMockito {
	BanqueUtilisateurs banqueUtilisateurs;
	@Before
	public void setUp() throws Exception {
		banqueUtilisateurs = mock(BanqueUtilisateurs.class);
	}
	
	@Test
	public void testGetListUtilisateursParRole(){
		ArrayList<Utilisateur> listeUtilisateurs = new ArrayList<>();
		Utilisateur client = mock(Utilisateur.class);
		listeUtilisateurs.add(client);
		when(banqueUtilisateurs.getListUtilisateursParRole("client")).thenReturn(listeUtilisateurs);
		assertEquals(listeUtilisateurs, banqueUtilisateurs.getListUtilisateursParRole("client"));
	}
	
	@Test
	public void testChercherUtilisateur(){
		Utilisateur client = mock(Utilisateur.class);
		when(banqueUtilisateurs.chercherUtilisateur(client.getNom())).thenReturn(client);
		assertEquals(client,banqueUtilisateurs.chercherUtilisateur(client.getNom()));
	}
}
