package testServices;

import java.io.IOException;
import java.util.ArrayList;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

import models.Requete;
import models.Utilisateur;
import models.Requete.Statut;
import services.BanqueRequetes;
import services.BanqueUtilisateurs;

public class TestBanqueUtilisteur {
	private BanqueUtilisateurs userList;
	private Utilisateur userTest;

	@Before
	public void setUp() throws IOException{
		userList = BanqueUtilisateurs.getUserInstance();

		userTest = new Utilisateur("Test", "root"); //Déjà dans la banqueUtilisateur.txt
		userList.getListeUtilisateurs().add(userTest);
	}

	@Test
	public void testGetListUtilisateurParRole(){
		String role = "client";
		ArrayList<Utilisateur> listUtils = userList.getListUtilisateursParRole(role);

		boolean flagErr = false;

		for (Utilisateur user: listUtils) {
			if(!user.getRole().equals(role))
				flagErr = true;
		}

		assertTrue(!flagErr);
	}

	@Test
	public void testChercherUtilisateur(){
		Utilisateur wanted = userList.chercherUtilisateur(userTest.getNom());

		assertEquals(userTest, wanted);
	}
}
