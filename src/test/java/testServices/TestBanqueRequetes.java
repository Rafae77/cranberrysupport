package testServices;

import static org.junit.Assert.*;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import models.Technicien;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import models.Requete;
import models.Utilisateur;
import models.Requete.Statut;
import services.BanqueRequetes;
import services.BanqueUtilisateurs;

public class TestBanqueRequetes {
	BanqueRequetes banqueRequetes;
	Utilisateur client;
	Requete.Categorie categorie;
	int nombreRequetesAjoutee;
	String sujet;
	String description;
	Statut defaultStatus;
	
	@Before
	public void setUp() throws Exception {
		banqueRequetes = BanqueRequetes.getInstance();
		client = new Utilisateur("test", "test");
		categorie = Requete.Categorie.compteUsager;
		sujet = "sujetTest";
		description = "descriptionTest";
		nombreRequetesAjoutee = 1;
		defaultStatus = Statut.ouvert;
		Requete requete = new Requete(sujet, description, client, categorie);
		banqueRequetes.newRequest(requete);
	}

	@After
	public void tearDown() throws Exception {
		banqueRequetes.resetListeRequetes();
		banqueRequetes.saveRequetes();
		banqueRequetes = null;
		client = null;
		nombreRequetesAjoutee = 0;
	}
	
	@Test
	public void testNewRequestSujet() throws IOException {
		assertEquals(sujet, banqueRequetes.getListeRequetes().get(0).getSujet());
	}
	
	@Test
	public void testNewRequestDescription() throws IOException {
		assertEquals(description, banqueRequetes.getListeRequetes().get(0).getDescription());
	}
	
	@Test
	public void testNewRequestClient() throws IOException {
		assertEquals(client, banqueRequetes.getListeRequetes().get(0).getClient());
	}
	
	@Test
	public void testNewRequestCategorie() throws IOException {
		assertEquals(categorie, banqueRequetes.getListeRequetes().get(0).getCategorie());
	}
	
	@Test
	public void testNewRequestAjoutDansLaListeRequetes() throws IOException {
		assertEquals(nombreRequetesAjoutee, banqueRequetes.getListeRequetes().size());
	}
	
	@Test
	public void testNewRequestAjoutDansLaListeRequetesDuClient() throws IOException {
		assertEquals(sujet, client.getListeRequetes().get(0).getSujet());
	}

	@Test
	public void testGetListRequetesParStatut() {
		assertEquals(banqueRequetes.getListeRequetes().get(0), banqueRequetes.getListRequetesParStatut(defaultStatus).get(0));
	}

	@Test
	public void testAssignerRequete() throws IOException{
		Technicien tech = new Technicien("tech", "root");
		Requete req = new Requete("suj", "desc", tech, Requete.Categorie.posteDeTravail);

		banqueRequetes.assignerRequete(tech, req);

		assertTrue(tech.getListeRequetesEnCours().contains(req)&&req.getStatut()== Statut.enTraitement);
	}
	
	@Test
	public void testReturnLast() {
		int lastRequete = banqueRequetes.getListeRequetes().size()-1;
		assertEquals(banqueRequetes.getListeRequetes().get(lastRequete), banqueRequetes.returnLast());
	}
	
	@Test
	public void testIncrementNumber() {
		int attendu = banqueRequetes.getNumero()+1;
		banqueRequetes.incrementNumber();
		assertTrue(attendu == banqueRequetes.getNumero());
	}

	@Test
	public void testSaveRequest() throws IOException, java.sql.SQLException{
		Utilisateur user = new Utilisateur("admin", "root");
		BanqueUtilisateurs.getUserInstance().getListeUtilisateurs().add(user);
		Requete reqTest = new Requete("testeux", "desc", user, Requete.Categorie.posteDeTravail);

		banqueRequetes.getListeRequetes().add(reqTest);

		banqueRequetes.saveRequetes();

		Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/tpSynthese?serverTimezone=America/New_York", "root", "");
		Statement stmt = con.createStatement();

		ResultSet result = stmt.executeQuery("SELECT * FROM requete WHERE sujet = 'testeux'");
		while(result.next())
			assertTrue(result.getString("sujet").equals("testeux"));
	}
}	
