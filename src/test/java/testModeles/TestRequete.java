package testModeles;

import static org.junit.Assert.*;

import models.Commentaire;
import models.Requete;
import models.Utilisateur;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class TestRequete {

	private Requete requete;
	private Utilisateur utilisateur;
	@Before
	public void setUp() throws Exception {
		utilisateur = new Utilisateur("admin", "root");
		requete = new Requete("sujet", "desc", utilisateur, Requete.Categorie.posteDeTravail);
	}

	@Test
	public void testAddCommantaire(){
		String commentaire = "un commantaire";
		requete.addCommentaire(commentaire, utilisateur);

		Commentaire com = requete.getComments().get(requete.getComments().size()-1);

		assertTrue(com.getComment().equals(commentaire) && com.getAuteur() == utilisateur);
	}

}
