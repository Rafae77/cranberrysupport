package testModeles;

import static org.junit.Assert.*;

import models.Requete;
import models.Utilisateur;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class TestUtilisateur {

	private Utilisateur user;
	private String motDePasse = "root";

	private Requete requete;

	@Before
	public void setUp() throws Exception {
		user = new Utilisateur("admin", motDePasse);
		requete = new Requete("sujet", "desc", user, Requete.Categorie.posteDeTravail);
	}

	@Test
	public void testLogin(){
		assertTrue(user.login(motDePasse));
	}

	@Test
	public void testAjoutRequete(){
		user.ajoutRequete(requete);
		Requete utilisateurRequete = user.getListeRequetes().get(user.getListeRequetes().size()-1);

		assertTrue( utilisateurRequete == requete);
	}

}
