package testModeles;

import static org.junit.Assert.*;

import models.Requete;
import models.Technicien;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class TestTechnicien {
	private Technicien technicien;
	private Requete requete;

	@Before
	public void setUp() throws Exception {
		technicien = new Technicien("admin", "root");
		requete = new Requete("sujet", "desc", technicien, Requete.Categorie.posteDeTravail);
	}

	@Test
	public void testAjouteRequeteAssigner(){
		technicien.ajouterRequeteAssignee(requete);

		Requete technicienRequeteEnCours = technicien.getListeRequetesEnCours().get(technicien.getListeRequetesEnCours().size()-1);
		Requete technicienRequete = technicien.getListeRequetes().get(technicien.getListeRequetes().size()-1);

		assertTrue( (technicienRequeteEnCours == requete)&&(technicienRequete == requete));
	}

	@Test
	public void testAjouteRequeteFinies(){
		technicien.ajouterRequeteAssignee(requete);
		technicien.ajoutListRequetesFinies(requete);

		Requete requeteFini = technicien.getListeRequetesFinies().get(technicien.getListeRequetesFinies().size()-1);

		assertTrue((requeteFini == requete)&&!technicien.getListeRequetesEnCours().contains(requete));
	}


	@Test
	public void testGetRequeteParStatut(){
		technicien.ajoutRequete(requete);

		String result = technicien.getRequeteParStatut();


	}

}
