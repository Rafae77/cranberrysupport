import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import services.BanqueRequetes;
import vues.CranberryFrame;

public class Main {
	static String appLook = "Nimbus";
	static BanqueRequetes banqueRequetes;

	public static void main(String args[]) throws IOException, java.sql.SQLException {
		banqueRequetes = BanqueRequetes.getInstance();

        setNimbusLookAndFeel();
        displayForm();
        saveRequests();
    }

	private static void saveRequests() {
		Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    banqueRequetes.saveRequetes();
                } catch (IOException ex) {
                    Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }));
	}

	private static void displayForm() {
		java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new CranberryFrame().setVisible(true);
            }
        });
	}
    
	private static void setNimbusLookAndFeel() {
		try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if (appLook.equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(CranberryFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(CranberryFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(CranberryFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(CranberryFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
	}
}
