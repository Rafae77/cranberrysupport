package services;


import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Scanner;

import models.Technicien;
import models.Utilisateur;

import javax.rmi.CORBA.Util;

public class BanqueUtilisateurs {

    private static BanqueUtilisateurs banqueUtilisateur = null;
    private ArrayList<Utilisateur> listeUtilisateurs;

    private BanqueUtilisateurs() throws FileNotFoundException{
        listeUtilisateurs = new ArrayList<Utilisateur>(100);
        chargerUtilisateur();
    }

    private void chargerUtilisateur() throws FileNotFoundException {
        Scanner banqueUtilisateurScanner = new Scanner(new FileReader("dat/BanqueUtilisateur.txt"));
        while (banqueUtilisateurScanner.hasNextLine()) {
            String line = banqueUtilisateurScanner.nextLine();
            String[] informationUtilisateur = line.split(";");
            this.addUtilisateur(informationUtilisateur[0], informationUtilisateur[1], informationUtilisateur[2]);
        }
    }

    
    public static BanqueUtilisateurs getUserInstance() throws FileNotFoundException {
        if(banqueUtilisateur == null)
            banqueUtilisateur = new BanqueUtilisateurs();
        return banqueUtilisateur;
    }

    public ArrayList getListUtilisateursParRole(String role) {
        ArrayList<Utilisateur> utilisateurs = new ArrayList<Utilisateur>(100);
        for (Utilisateur utilisateur: listeUtilisateurs) {
            if(utilisateur.getRole().equals(role))
                utilisateurs.add(utilisateur);
        }
        return utilisateurs;
    }

    public ArrayList getListeUtilisateurs(){
        return listeUtilisateurs;
    }

    public Utilisateur chercherUtilisateur(String utilisateurRechercher) {
        for (Utilisateur utilisateur: listeUtilisateurs) {
            if(utilisateur.getNom().equals(utilisateurRechercher))
                return utilisateur;
        }
        return null;
    }

    private void addUtilisateur(String nomUtilisateur, String motDePasse, String role) {
        if (role.equals("client")) {
            Utilisateur client = new Utilisateur(nomUtilisateur, motDePasse);
            listeUtilisateurs.add(client);
        } else {
            Technicien tech = new Technicien(nomUtilisateur, motDePasse);
            listeUtilisateurs.add(tech);
        }
    }
}