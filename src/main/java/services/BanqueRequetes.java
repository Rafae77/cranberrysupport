package services;

import java.io.IOException;
import java.util.ArrayList;

import models.Requete;
import models.Technicien;
import models.Utilisateur;

import java.sql.*;

public class BanqueRequetes {
	private static BanqueRequetes instance = null;
	private Integer numero = -1;
	private ArrayList<Requete> listeRequetes;

	private BanqueRequetes() throws IOException{
		listeRequetes = new ArrayList<Requete>();
		createEnvironement();
	}

	public static BanqueRequetes getInstance() throws IOException{
		if (instance == null) {
			instance = new BanqueRequetes();
			instance.chargerRequetes();
		}
		return instance;
	}

	public void newRequest(Requete requete) throws IOException{
		listeRequetes.add(requete);
		requete.getClient().ajoutRequete(requete);
	}

	public ArrayList<Requete> getListRequetesParStatut(Requete.Statut statut) {
		ArrayList<Requete> requetes = new ArrayList<Requete>();
		for (Requete requete: listeRequetes) {
			if(requete.getStatut().equals(statut))
				requetes.add(requete);
		}
		return requetes;
	}

	public void assignerRequete(Technicien technicien, Requete selectionee) {
		selectionee.setStatut(Requete.Statut.enTraitement);
		technicien.ajouterRequeteAssignee(selectionee);
	}

	public Requete returnLast() {
		return listeRequetes.get(listeRequetes.size() - 1);
	}

	public Integer incrementNumber() {
		return ++numero;
	}

	private void createEnvironement(){
		try{
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/?serverTimezone=America/New_York", "root", "");
			Statement stmt = con.createStatement();

			stmt.executeUpdate("CREATE DATABASE IF NOT EXISTS tpSynthese;");

			stmt.execute("use tpSynthese;");

			stmt.executeUpdate("CREATE TABLE IF NOT EXISTS requete(sujet VARCHAR(255), description VARCHAR(255), id INTEGER, client VARCHAR(255), categorie VARCHAR(255))");

			con.close();

		}catch (java.sql.SQLException e){
			System.out.println(e.getMessage());
		}
	}

	private void chargerRequetes() throws IOException{
		try{
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/tpSynthese?serverTimezone=America/New_York", "root", "");
			Statement stmt = con.createStatement();
			ResultSet result = stmt.executeQuery("SELECT * FROM requete");

			while(result.next()){
				Utilisateur user = BanqueUtilisateurs.getUserInstance().chercherUtilisateur(result.getString("client"));
				this.newRequest(new Requete(result.getString("sujet"), result.getString("description"),user, Requete.Categorie.fromString(result.getString("categorie"))));
			}

			con.close();
		}catch (java.sql.SQLException e){
			System.out.println(e.getMessage());
		}
	}
	
	public void saveRequetes() throws IOException {
		try{
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/tpSynthese?serverTimezone=America/New_York", "root", "");
			Statement stmt = con.createStatement();

			stmt.executeUpdate("DELETE FROM requete;");

			for (Requete req: listeRequetes) {
				stmt.executeUpdate("INSERT INTO requete VALUES('"+req.getSujet()+"','"+req.getDescription()+"','"+req.getNumero()+"','"+req.getClient().getNom()+"','"+req.getCategorie().getValueString()+"');");
			}
			con.close();
		}catch (java.sql.SQLException e){
			System.out.println(e.getMessage());
		}
	}

	public Integer getNumero() {
		return numero;
	}

	public ArrayList<Requete> getListeRequetes() {
		return listeRequetes;
	}	
	
	public void resetListeRequetes() {
		listeRequetes.clear();
	}	
}
