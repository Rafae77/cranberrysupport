package models;


import java.util.ArrayList;

public class Technicien extends Utilisateur {
    private ArrayList<Requete> ListeRequetesFinies;
    private ArrayList<Requete> ListeRequetesEnCours;

    public Technicien(String nom, String motDePasse) {
        super(nom, motDePasse);
        this.role = "technicien";
        ListeRequetesEnCours = new ArrayList<Requete>();
        ListeRequetesFinies = new ArrayList<Requete>();
    }

    public ArrayList<Requete> getListeRequetesFinies() {
        return ListeRequetesFinies;
    }

    public ArrayList<Requete> getListeRequetesEnCours() {
        return ListeRequetesEnCours;
    }

    public void ajouterRequeteAssignee(Requete assignee) {
        ListeRequetesEnCours.add(assignee);
        super.listeRequetes.add(assignee);
    }

    public void ajoutListRequetesFinies(Requete finie) {
        ListeRequetesFinies.add(finie);
        ListeRequetesEnCours.remove(finie);
    }

    public String getRequeteParStatut() {
        String ouvert = "Statut ouvert: ";
        int ouvertCompteur = 0;
        String enTraitement = "Statut En traitement: ";
        int enTraitementCompteur = 0;
        String succes = "Statut succès: ";
        int succesCompteur = 0;
        String abandon = "Statut abandonnée: ";
        int abandonCompteur = 0;
        String statut = "";

        for ( Requete req: super.listeRequetes) {
            if (req.getStatut().equals(Requete.Statut.ouvert)) {
                ouvertCompteur++;
            }
            if (req.getStatut().equals(Requete.Statut.enTraitement)) {
            	enTraitementCompteur++;
            }
            if (req.getStatut().equals(Requete.Statut.finalSucces)) {
                succesCompteur++;
            }
            if (req.getStatut().equals(Requete.Statut.finalAbandon)) {
                abandonCompteur++;
            }
        }

        statut = ouvert + ouvertCompteur + "\n"
                + enTraitement + enTraitementCompteur + "\n"
                + succes + succesCompteur + "\n"
                + abandon + abandonCompteur + "\n";
        return statut;

    }
}