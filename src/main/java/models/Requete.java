package models;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import services.BanqueRequetes;

public class Requete {
	private String sujet;
    private String description;
    private File fichier;
    private Integer numero;
    private Utilisateur client;
    private Statut statut;
    private Categorie categorie;
    private ArrayList<Commentaire> listeCommentaires;
   
    public Requete(String sujet, String description, Utilisateur utilisateur, Categorie categorie)throws IOException{
        this.sujet = sujet;
        this.description = description;
        this.client = utilisateur;
        this.numero = BanqueRequetes.getInstance().incrementNumber();
        this.statut = Statut.ouvert;
        this.categorie = categorie;
        this.listeCommentaires = new ArrayList<Commentaire>();
    }

    public Requete(String sujet, String description, Utilisateur utilisateur, Categorie categorie, String fichier) throws IOException{
        this.sujet = sujet;
        this.description = description;
        this.client = utilisateur;
        this.fichier = new File(fichier);
        this.numero = BanqueRequetes.getInstance().incrementNumber();
        this.statut = Statut.ouvert;
        this.categorie = categorie;
        this.listeCommentaires = new ArrayList<Commentaire>();
    }

    public String getSujet() {
        return sujet;
    }

    public void setSujet(String nouveau) {
        sujet = nouveau;
    }
    
    public void setDescription(String nouvelle) {
        description = nouvelle;
    }

    public void setCategorie(Categorie choix) {
        categorie = choix;
    }

    public File getFichier() {
        return fichier;
    }

    public void setStatut(Statut newstat) {
        statut = newstat;
    }

    public void addCommentaire(String commentaire, Utilisateur utilisateur) {
        Commentaire suivant = new Commentaire(commentaire, utilisateur);
        listeCommentaires.add(suivant);
    }

    public void setFile(File file) {
        fichier = file;
    }
    
    public Integer getNumero() {
        return numero;
    }

    public Statut getStatut() {
        return statut;
    }

    public ArrayList<Commentaire> getComments() {
        return listeCommentaires;
    }

    public Categorie getCategorie() {
        return categorie;
    }

    public Utilisateur getClient() {
        return client;
    }

    public String getDescription() {
        return description;
    }
    
    public enum Statut {
        ouvert("ouvert"), enTraitement("en traitement"), finalAbandon("Abandon"), finalSucces("Succès");
        String valeur;

        Statut(String s) {
            this.valeur = s;
        }

        public String getValueString() {
            return valeur;
        }

        public static Statut fromString(String text) {
            if (text != null) {
                if (Statut.ouvert.getValueString().equals(text)) {
                    return Statut.ouvert;
                } else if (Statut.enTraitement.getValueString().equals(text)) {
                    return Statut.enTraitement;
                } else if (Statut.finalAbandon.getValueString().equals(text)) {
                    return Statut.finalAbandon;
                } else if (Statut.finalSucces.getValueString().equals(text)) {
                    return Statut.finalSucces;
                } else if (Statut.finalAbandon.getValueString().equals(text)) {
                    return Statut.finalAbandon;
                }
            }
            return null;
        }
    }

    public enum Categorie {
        posteDeTravail("Poste de travail"), serveur("Serveur"),
        serviceWeb("Service web"), compteUsager("Compte usager"), autre("Autre");
        String value;

        Categorie(String s) {
            this.value = s;
        }

        public String getValueString() {
            return value;
        }

        public static Categorie fromString(String text) {
            if (text != null) {
                if (Categorie.posteDeTravail.getValueString().equals(text)) {
                    return Categorie.posteDeTravail;
                } else if (Categorie.serveur.getValueString().equals(text)) {
                    return Categorie.serveur;
                } else if (Categorie.serviceWeb.getValueString().equals(text)) {
                    return Categorie.serviceWeb;
                } else if (Categorie.compteUsager.getValueString().equals(text)) {
                    return Categorie.compteUsager;
                } else if (Categorie.autre.getValueString().equals(text)) {
                    return Categorie.autre;
                }
            }
            return null;
        }
    }
}