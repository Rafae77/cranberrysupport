package models;
public class Commentaire {

    private String commentaire;
    private Utilisateur auteur;
    
    public Commentaire(String commentaire, Utilisateur auteur) {
        this.commentaire = commentaire;
        this.auteur = auteur;
    }

    public Utilisateur getAuteur() {
        return auteur;
    }

    public String getComment() {
        return commentaire;
    }

    public String toString() {
        return getAuteur().getNom() + ": " + getComment() + "\n";
    }
}