package models;

import java.util.ArrayList;

public class Utilisateur {
    private String nomUtilisateur;
    private String motDePasse;
    protected String role;
    protected ArrayList<Requete> listeRequetes;
    protected ArrayList<Requete> listeRequetesPersonnes;

    public Utilisateur(String nomUtilisateur, String motDePasse) {
        this.nomUtilisateur = nomUtilisateur;
        this.motDePasse = motDePasse;
        this.role = "client";

        listeRequetes = new ArrayList<Requete>();
        listeRequetesPersonnes = new ArrayList<Requete>();
    }

    public String getRole() {
        return role;
    }

    public String getNom() {
        return nomUtilisateur;
    }

    public boolean login(String motdp) {
        return  motDePasse.equals(motdp);
    }

    public ArrayList<Requete> getListeRequetes(){ return listeRequetes; }

    public void ajoutRequete(Requete nouvelle){
        listeRequetes.add(nouvelle);
        listeRequetesPersonnes.add(nouvelle);
    }

    public ArrayList<Requete> getListPersonnes(){ return listeRequetesPersonnes; }
}